package com.momo.momoshow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MomoShowApplication {

    public static void main(String[] args) {
        SpringApplication.run(MomoShowApplication.class, args);
    }

}
